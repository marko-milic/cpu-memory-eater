package capacity;

public enum Unit {
    BYTE(1),
    KILOBYTE(BYTE.factor * 1024),
    MEGABYTE(KILOBYTE.factor * 1024),
    GIGABYTE(MEGABYTE.factor * 1024),
    TERABYTE(GIGABYTE.factor * 1024);

    private long factor;

    Unit(long factor) {
        this.factor = factor;
    }

    public long getFactor() {
        return factor;
    }
}
