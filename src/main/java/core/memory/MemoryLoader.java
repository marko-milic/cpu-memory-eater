package core.memory;

import core.Loadable;

public class MemoryLoader implements Loadable {
    private long bytesToLoad;

    private MemoryLoader() {}

    public static MemoryLoader create() {
        return new MemoryLoader();
    }

    public MemoryLoader withBytesToLoad(long bytesToLoad) {
        this.bytesToLoad = bytesToLoad;
        return this;
    }

    @Override
    public void load() {
        new Thread(() -> new MemoryLoad(bytesToLoad).load()).start();
    }
}
