package core;

public interface Loadable {

    void load();
}
