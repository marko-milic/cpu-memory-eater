package core;

import java.util.LinkedList;
import java.util.List;

public class LoadManager {
    private static LoadManager instance;
    private List<Loadable> loaders;

    public static LoadManager getInstance() {
        if (null == instance)
            instance = new LoadManager();

        return instance;
    }

    private LoadManager() {
        this.loaders = new LinkedList<>();
    }

    public LoadManager register(Loadable loader) {
        loaders.add(loader);
        return this;
    }

    public LoadManager startLoaders() {
        for (Loadable loader : loaders) {
            loader.load();
        }
        return this;
    }
}
