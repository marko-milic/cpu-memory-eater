package core.cpu;

import core.Loadable;

public class CPULoader implements Loadable {
    private double loadPerCore;
    private int cores;

    public static CPULoader create() {
        return new CPULoader();
    }

    public CPULoader withLoad(double loadPerCore) {
        this.loadPerCore = loadPerCore;
        return this;
    }

    public CPULoader withCores(int cores) {
        this.cores = cores;
        return this;
    }

    @Override
    public void load() {
        for (int thread = 0; thread < cores; thread++) {
            new Thread(() -> new CoreLoad(loadPerCore).load())
                    .start();
        }
    }
}
