package core.cpu;

public class CoreLoad {
    private double load;

    public CoreLoad(double load) {
        this.load = load;
    }

    public void load() {
        System.out.println("Hello from CPU Loader!" + " THREAD: " + Thread.currentThread().getName());
        while (true) {
            if (System.currentTimeMillis() % 100 == 0) {
                try {
                    Thread.sleep((long) Math.floor((1 - load) * 100));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}


